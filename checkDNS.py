#!/usr/bin/env python
#
# Module for testing DNS labs of students.
# Author: Kosenko Maxim
#

import DNS
import re
import sys
import string
'''
# name_pod: [first_ip, second_ip, domain_h1, domain_h2, domain_www, domain_file, domain_mail, domain_monit, pod_id, slave_pod_id]
#		0	1		2	3	4		5		6	7		8	9
machines = {}
ip = int(111)
count_of_hosts = 19
for id_pod in range(1,count_of_hosts+1):
	name_pod = 'pod' + str(id_pod)
	first_ip = '192.168.88.' + str(ip)
	ip += 1
	second_ip = '192.168.88.' + str(ip)
	ip += 1
	domain_h1 = 'h1.' + name_pod + '.ru'
	domain_h2 = 'h2.' + name_pod + '.ru'
	domain_www = 'www.' + name_pod + '.ru'
	domain_file = 'file.' + name_pod + '.ru'
	domain_mail = 'mail.' + name_pod + '.ru'
	domain_monit = 'monit.' + name_pod + '.ru'
	pod_id = id_pod
	slave_pod_id = id_pod-1 if id_pod-1 else count_of_hosts
	machines.update( {name_pod: [first_ip, second_ip, domain_h1, domain_h2, domain_www, domain_file, domain_mail, domain_monit, pod_id, slave_pod_id]} )

teacher_ip = '192.168.88.100'
'''
def make_ptr(ip):	# ip =  machines[key][0]
	ptr = ip
	ptr = string.split(ptr, '.')
	ptr.reverse()
	ptr = string.join(ptr, '.') + '.in-addr.arpa'
	return ptr

def check_dns(machines, teacher_ip):
	DNS.DiscoverNameServers()
	DNS.defaults['timeout'] = 5
	keys = machines.keys()
	if len(sys.argv) >= 2:
		keys = ['pod'+str(sys.argv[i]) for i in range(1,len(sys.argv))]
	for key in keys:
		print('[.]')
		print("[.] CHECK %s" % key)
		print('[.] Check DNS Server')
		print('[.] Checking domain names resolve:')
		check_domain(key+'.iit.ru', machines[key][1], machines[key][0])		# podN.iit.ru
		check_domain(machines[key][2], machines[key][1], machines[key][0])	# h1
		check_domain(machines[key][3], machines[key][1], machines[key][1])	# h2
		check_domain(machines[key][4], machines[key][1], machines[key][0])	# www
		check_domain(machines[key][5], machines[key][1], machines[key][0])	# file
		check_domain(machines[key][6], machines[key][1], machines[key][1])	# mail
		check_domain(machines[key][7], machines[key][1], machines[key][1])	# monit
		print('[.] Checking MX records:')
		check_domain(key+'.iit.ru', machines[key][1], machines[key][0], qtype='MX', mx_domain=machines[key][6])	# podN.iit.ru MX check
		print('[.] Checking reverse DNS:')
		ptr = make_ptr(machines[key][0])	#	first machine
		check_domain(ptr, machines[key][1], machines[key][0], qtype='PTR', ptr_domain=machines[key][2])
		ptr = make_ptr(machines[key][1])	#	second machine
		check_domain(ptr, machines[key][1], machines[key][1], qtype='PTR', ptr_domain=machines[key][3])
		ptr = make_ptr(teacher_ip)	#	teacher machine
		check_domain(ptr, machines[key][1], teacher_ip, qtype='PTR', ptr_domain='teacher.'+key+'.ru')
		print('[.] Checking SLAVE zone:')
		slave_pod_key = 'pod' + str( int(machines[key][9]) )
		check_domain( slave_pod_key+'.iit.ru', machines[key][1], machines[slave_pod_key][0])		# podN.iit.ru
		check_domain(machines[slave_pod_key][2], machines[key][1], machines[slave_pod_key][0])	# h1
		check_domain(machines[slave_pod_key][3], machines[key][1], machines[slave_pod_key][1])	# h2
		check_domain(machines[slave_pod_key][4], machines[key][1], machines[slave_pod_key][0])	# www
		check_domain(machines[slave_pod_key][5], machines[key][1], machines[slave_pod_key][0])	# file
		check_domain(machines[slave_pod_key][6], machines[key][1], machines[slave_pod_key][1])	# mail
		check_domain(machines[slave_pod_key][7], machines[key][1], machines[slave_pod_key][1])	# monit
		print('[.] Checking SLAVE MX records:')
		check_domain(slave_pod_key+'.iit.ru', machines[key][1], machines[slave_pod_key][0], qtype='MX', mx_domain=machines[slave_pod_key][6])	# podN.iit.ru MX check
		print('[.] Checking AXFR:')
		check_domain( key+'.iit.ru', machines[key][1], machines[key][0], qtype='AXFR')		# podN.iit.ru
		
#		print('[.] Checking SLAVE reverse DNS:')
#		ptr = make_ptr(machines[slave_pod_key][0])	#	first machine
#		check_domain(ptr, machines[key][1], machines[slave_pod_key][0], qtype='PTR', ptr_domain=machines[slave_pod_key][2])
#		ptr = make_ptr(machines[slave_pod_key][1])	#	second machine
#		check_domain(ptr, machines[key][1], machines[slave_pod_key][1], qtype='PTR', ptr_domain=machines[slave_pod_key][3])
#		ptr = make_ptr(teacher_ip)	#	teacher machine
#		check_domain(ptr, machines[key][1], teacher_ip, qtype='PTR', ptr_domain='teacher.'+slave_pod_key+'.ru')


def check_domain(domain, ip_dns_server, ip_service, qtype='A', mx_domain='', ptr_domain=''):
	try:
		resp = DNS.Request(name=domain, server=ip_dns_server, qtype=qtype).req()
	except DNS.Base.TimeoutError:
		print("[!] DNS Server not work: TIMEOUT Error")
		sys.exit('DNS Server not work: TIMEOUT Error')
	except DNS.Base.SocketError: 
		print("[!] DNS Server not work: Connection refused")
		sys.exit('DNS Server not work: Connection refused')
	if qtype == 'A':
		if resp.answers == [] : 
			print("[!] %s : IP addres not found" % (domain))
			return False
		elif resp.answers[0]['data'] == ip_service :
			print("[*] %s : %s" % (domain, resp.answers[0]['data']))
		else:
			print("[!] %s : IP addres not match (%s)" % (domain, resp.answers[0]['data']))
	elif qtype == 'MX':
		if resp.answers != []:
			if resp.answers[0]['data'][0] == 10:
				if resp.answers[0]['data'][1] == mx_domain:
					print('[*] MX checking correct')
					print('    %s' % (resp.answers[0]['data'][1]))
				else:
					print('[!] MX domain name is wrong : %s' % resp.answers[0]['data'][1])
		else:
			print('[!] MX fail')
	elif qtype == 'PTR':
		if resp.answers != []:
			if resp.answers[0]['data'] == ptr_domain:
				print("[*] %s : %s" % (domain, resp.answers[0]['data']))
			else:
				print('[!] Domain name is wrong : %s' % resp.answers[0]['data'])
		else:
			print('[!] PTR fail')
	elif qtype == 'AXFR':
		if resp.answers != []:
			print('[!] AXFR allow')
		else:
			print('[*] AXFR disallow')

	return True


if __name__ == '__main__':
#	if len(sys.argv) > 1:
#		testOne(format(sys.argv[1]), machines[format(sys.argv[1])][0], machines[format(sys.argv[1])][1])
#	else:
#		main()
	check_dns()
