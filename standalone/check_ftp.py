#!/usr/bin/env python
#
# Module for testing DNS labs of students.
# Author: Kosenko Maxim
#

import re
import sys
import string
import os
from ftplib import FTP

# name_pod: [first_ip, second_ip, domain_h1, domain_h2, domain_www, domain_file, domain_mail, domain_monit, pod_id, slave_pod_id, name_pod]
#		0	1		2	3	4		5		6	7		8	9		10
machines = {}
ip = int(111)
count_of_hosts = 19
for id_pod in range(1,count_of_hosts+1):
	name_pod = 'pod' + str(id_pod)
	first_ip = '192.168.88.' + str(ip)
	ip += 1
	second_ip = '192.168.88.' + str(ip)
	ip += 1
	domain_h1 = 'h1.' + name_pod + '.ru'
	domain_h2 = 'h2.' + name_pod + '.ru'
	domain_www = 'www.' + name_pod + '.ru'
	domain_file = 'file.' + name_pod + '.ru'
	domain_mail = 'mail.' + name_pod + '.ru'
	domain_monit = 'monit.' + name_pod + '.ru'
	pod_id = id_pod
	slave_pod_id = id_pod-1 if id_pod-1 else count_of_hosts
	machines.update( {name_pod: [first_ip, second_ip, domain_h1, domain_h2, domain_www, domain_file, domain_mail, domain_monit, pod_id, slave_pod_id, name_pod]} )

teacher_ip = '192.168.88.100'

def check_ftp():
        keys = machines.keys()
        if len(sys.argv) >= 2:
                keys = ['pod'+str(sys.argv[i]) for i in range(1,len(sys.argv))]
        for key in keys:
                print('[.]')
                print("[.] CHECK %s" % key)
                print('[.] Check FTP Server')
		
		get_ftp(machines[key][0], true_content=machines[key][10])
		get_ftp(machines[key][0], user='teacher', password='teacher', true_content=machines[key][10])
#		get_ftp(machines[key][0], 8080, key)

	return 0

def get_ftp(ip_address, user='anonymous', password='anonynous@', file_name='ftp_file', true_content=''):
        print('[.] Checking access: %s' % user)

#	Get file from FTP
#	print('[DEBUG] IP: %s' % ip_address)
	ftp = FTP(ip_address)
	ftp.login(user, password)

	out = ftp_read(ftp, ip_address, file_name)

	with open(out, 'r') as content_file:
		content = content_file.read()
#	Check file content
	match = re.search(true_content, content, re.IGNORECASE)
	if match:
		print('[.] Content of file: %s' % match.group())
		if match.group().lower() == true_content:
			print('[*] %s has read access.' % user)
			print('[*] File content is correct.')
	else:
		print("[!] File content has bad data!")

#	Write file to FTP	
#        ftp = FTP(ip_address)
#        ftp.login()

	path = '/root/.ssh/teacher_pub'
#	path = '/tmp/yum.log'
	name = 'teacher_pub'
	ftp_upload(ftp, path, user, name)

        ftp.quit()

def ftp_read(ftp_obj, ip_address, file_name):
	try:
		out = ip_address + '_' + file_name
		if os.path.exists(out):
			os.remove(out)

		with open(out, 'wb') as f:
			ftp_obj.retrbinary('RETR ' + file_name, f.write)
	except Exception, e:
		print('[!] %s' % str(e))

	return out

def ftp_upload(ftp_obj, path, user, name, ftype='TXT'):
	try:
		if ftype == 'TXT':
			with open(path) as fobj:
				ftp_obj.storlines('STOR ' + name, fobj)
		else:
			with open(path, 'rb') as fobj:
				ftp_obj.storbinary('STOR ', fobj + name, 1024)
		print('{*} File successfully uploaded.')
	except Exception, e:
		print('[=] For user %s : %s' % (user, str(e)) )


if __name__ == '__main__':
#	if len(sys.argv) > 1:
#		testOne(format(sys.argv[1]), machines[format(sys.argv[1])][0], machines[format(sys.argv[1])][1])
#	else:
#		main()
	check_ftp()
