#!/usr/bin/env python
#
# Module for testing DNS labs of students.
# Author: Kosenko Maxim
#

import re
import sys
import string
import requests

# name_pod: [first_ip, second_ip, domain_h1, domain_h2, domain_www, domain_file, domain_mail, domain_monit, pod_id, slave_pod_id]
#		0	1		2	3	4		5		6	7		8	9
machines = {}
ip = int(111)
count_of_hosts = 19
for id_pod in range(1,count_of_hosts+1):
	name_pod = 'pod' + str(id_pod)
	first_ip = '192.168.88.' + str(ip)
	ip += 1
	second_ip = '192.168.88.' + str(ip)
	ip += 1
	domain_h1 = 'h1.' + name_pod + '.ru'
	domain_h2 = 'h2.' + name_pod + '.ru'
	domain_www = 'www.' + name_pod + '.ru'
	domain_file = 'file.' + name_pod + '.ru'
	domain_mail = 'mail.' + name_pod + '.ru'
	domain_monit = 'monit.' + name_pod + '.ru'
	pod_id = id_pod
	slave_pod_id = id_pod-1 if id_pod-1 else count_of_hosts
	machines.update( {name_pod: [first_ip, second_ip, domain_h1, domain_h2, domain_www, domain_file, domain_mail, domain_monit, pod_id, slave_pod_id]} )

teacher_ip = '192.168.88.100'

def check_web():
        keys = machines.keys()
        if len(sys.argv) >= 2:
                keys = ['pod'+str(sys.argv[i]) for i in range(1,len(sys.argv))]
        for key in keys:
                print('[.]')
                print("[.] CHECK %s" % key)
                print('[.] Check WEB Server')
		
		get_web(machines[key][0], 80, 'php license')
		get_web(machines[key][0], 8080, key)

	return 0

def get_web(ip_address, port, true_result):
        print('[.] Checking service on port {}:'.format(port))
	domain = str( 'http://{}:{}'.format(ip_address,port) )
#	print('[debug] %s' % true_result)
	try:
		resp = requests.get(domain, timeout=5)
	except requests.exceptions.ConnectionError as e:
		print("[!] Web Server not work: ConnectionsError")
	except requests.exceptions.HTTPError as e:
		print("[!] Web Server not work: HTTPError")
	except requests.exceptions.Timeout as e:
		print("[!] Web Server not work: Timeout")
	else:
		match = re.search(true_result, resp.text, re.IGNORECASE)
		if match:
			print('[.] Content of main page: %s' % match.group())
			if match.group().lower() == true_result:
				print("[*] Web server is fine!")
		else:
			print("[!] Web server giveing the bad data!")

if __name__ == '__main__':
#	if len(sys.argv) > 1:
#		testOne(format(sys.argv[1]), machines[format(sys.argv[1])][0], machines[format(sys.argv[1])][1])
#	else:
#		main()
	check_web()
