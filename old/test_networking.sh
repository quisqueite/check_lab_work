#!/bin/bash
#
#

PING=`which ping`
IFCONFIG=`which ifconfig`
ETH="eth1"

#pingResult=`$PING -c 4 8.8.8.8 | grep loss`
pingResult=`$PING -c 4 10.123.123.8 | grep loss`

transmitted=`echo $pingResult | awk '{print $1}'`
received=`echo $pingResult | awk '{print $4}'`
loss=`echo $pingResult | awk '{print $6}'`
#echo "$transmitted $received $loss"

if [ x"$loss" = x"100%" ]; then
        ifconfigResult=`$IFCONFIG -a | grep $ETH`
        if [ -z "$ifconfigResult" ]; then reboot; fi
else
        echo Vse OK
fi

