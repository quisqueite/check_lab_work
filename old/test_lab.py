#!/usr/bin/python
#
# Script for testing DNS and Web servers of students.
# Author: Kosenko Maxim
#

import DNS
import requests
import re
import paramiko
import sys

dnsFlag = True
machines = {    'pod1': ['10.1.18.111', '10.1.18.112'], \
       	        'pod2': ['10.1.18.121', '10.1.18.122'], \
               	'pod3': ['10.1.18.131', '10.1.18.132'], \
                'pod4': ['10.1.18.141', '10.1.18.142'], \
       	        'pod5': ['10.1.18.151', '10.1.18.152'], \
                'pod6': ['10.1.18.161', '10.1.18.162'], \
                'pod7': ['10.1.18.171', '10.1.18.172'], \
                'pod8': ['10.1.18.181', '10.1.18.182'], \
                'pod9': ['10.1.18.191', '10.1.18.192'], \
		'pod10': ['10.1.18.201', '10.1.18.202'] }

def main():
	DNS.DiscoverNameServers()
	DNS.defaults['timeout'] = 5
	keys = list(machines)
	for key in keys:
		print("\n    CHECK %s" % key)
#		dnsFlag = True
		print("Check DNS Server:")
        	checkingDomain = str('www.'+key+'.ru')
		dnsFlag = checkDNS(checkingDomain, machines[key][1], machines[key][0])
		if dnsFlag:
			checkingDomain = str('h1.'+key+'.ru')
			checkDNS(checkingDomain, machines[key][1], machines[key][0])
			checkingDomain = str('h2.'+key+'.ru')
			checkDNS(checkingDomain, machines[key][1], machines[key][1])
			checkingDomain = str('file.'+key+'.ru')
			checkDNS(checkingDomain, machines[key][1], machines[key][0])
			checkWEB(key, machines[key][0])
			checkSSH(machines[key][0], False)
			checkSSH(machines[key][1], False)
			checkSSH(machines[key][0], True)
			checkSSH(machines[key][1], True)
		else:
			print("WEB server not checks")

def checkDNS(domain, ipDNS, ipServer):
#	print("Check DNS Server %s" % (str(podName)))
#	domain = str('www.'+podName+'.ru')
	try:
		resp = DNS.Request(name=domain, server=ipDNS).req()
	except DNS.Base.TimeoutError:
		print("DNS Server not work: TIMEOUT Error")
		return False
	except DNS.Base.SocketError: 
		print("DNS Server not work: Connection refused")
		return False
	if resp.answers == [] : 
		print("%s : IP addres not found" % (domain))
		return False
	elif resp.answers[0]['data'] == ipServer :
		print("%s : %s" % (domain, resp.answers[0]['data']))
	else:
		print("IP addres not match (%s)" % (resp.answers[0]['data']))
	return True

def checkWEB(podName, ipServer):
	print("Check WEB Server %s" % (podName))
#	domain = str('http://www.'+podName+'.ru')
	domain = str('http://'+ipServer)
	try:
		resp = requests.get(domain)
	except requests.exceptions.ConnectionError as e:
#		print("\tError code: %s" % e.code)
		print("ConnectionsError")
	except requests.exceptions.HTTPError as e:
#		print("\tError code: %s" % e.code)
		print("HTTPError")
	except requests.exceptions.Timeout as e:
#		printf("\tError code: %s" % e.code)
		print("Timeout")
	else:
		match = re.search(podName, resp.text)
		if match:
			print(match.group())
			if match.group() == podName:
				print("Web server is fine!")
		else:
			print("Web server giveing the bad data!")

def checkSSH(sshServer, useKey):
	ssh = paramiko.SSHClient()
	ssh.set_missing_host_key_policy(paramiko.AutoAddPolicy())
	if useKey:
		print("Testing SSH for %s with key" % sshServer)
		try:
			print(sshServer)
	                ssh.connect(sshServer, username='teacher', key_filename='/home/admin/.ssh/admin_rsa')
	                stdin, stdout, stderr = ssh.exec_command('whoami')
        	        whoami = stdout.readlines()
			print(who)
                	if 'teacher\n' == who:
                        	print("Auth with key successfull")		
		except paramiko.ssh_exception.SSHException:
			print("Not a valid EC private key file")
		except:
			print("EXCEPTION SSH with key")
#			print(paramiko.ssh_exception.SSHException)
	else:
                print("Testing SSH for %s without key" % sshServer)
		try:
	                ssh.connect(sshServer, username='teacher', password='teacher')
        	        stdin, stdout, stderr = ssh.exec_command('groups')
			groups = stdout.readlines()
#			if len(groups) == 2:
			for gr in groups:
				print(gr)
#			if 'administrators' in groups:
#                               	print("teacher:administrators TRUE")
#			else:
#				print("teacher:administrators FALSE")
#                        if 'teachers' in groups:
#       	                        print("teacher:teachers TRUE")
#			else:
#				print("teacher:teachers FALSE")
#                	else:
#                        	print("Teacher not included in the two groups. %i" % len(groups))
		except:
			print("EXCEPTION SSH")
	ssh.close()

#def checkFS(ipServer):
def testOne(key, ipServer, ipDNS):
        DNS.DiscoverNameServers()
        DNS.defaults['timeout'] = 5
        print("\n    CHECK %s" % key)
#               dnsFlag = True
        print("Check DNS Server:")
        checkingDomain = str('www.'+key+'.ru')
        dnsFlag = checkDNS(checkingDomain, ipDNS, ipServer)
        if dnsFlag:
		checkingDomain = str('h1.'+key+'.ru')
		checkDNS(checkingDomain, ipDNS, ipServer)
		checkingDomain = str('h2.'+key+'.ru')
		checkDNS(checkingDomain, ipDNS, ipServer)
		checkingDomain = str('file.'+key+'.ru')
		checkDNS(checkingDomain, ipDNS, ipServer)
#		checkWEB(key)
		checkWEB(key, ipServer)
		checkSSH(ipServer, False)
		checkSSH(ipDNS, False)
		checkSSH(ipServer, True)
		checkSSH(ipDNS, True)
	else:
		print("WEB server not checks")

if __name__ == '__main__':
	if len(sys.argv) > 1:
		testOne(format(sys.argv[1]), machines[format(sys.argv[1])][0], machines[format(sys.argv[1])][1])
	else:
		main()
