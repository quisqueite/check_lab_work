#!/usr/bin/env python
#
# Module for testing lab.
# Author: Kosenko Maxim
#

import sys
import paramiko
from checkDNS import *
from checkWEB import *

def check_ssh(sshServer, useKey):
        ssh = paramiko.SSHClient()
        ssh.set_missing_host_key_policy(paramiko.AutoAddPolicy())
        if useKey:
                print('[.] Testing SSH for %s with key' % sshServer)
                try:
#                        print(sshServer)
                        ssh.connect(sshServer, username='teacher', key_filename='/home/root/.ssh/teacher')
                        stdin, stdout, stderr = ssh.exec_command('whoami')
                        whoami = stdout.readlines()
                        print(whoami)
                        if 'teacher\n' == whoami:
                                print('[*] Auth with key successfull')
                except paramiko.ssh_exception.SSHException:
                        print('[!] Not a valid EC private key file')
                except:
                        print('[!] EXCEPTION SSH with key')
#                       print(paramiko.ssh_exception.SSHException)
        else:
                print('[.] Testing SSH for %s without key' % sshServer)
                try:
                        ssh.connect(sshServer, username='teacher', password='teacher')
                        stdin, stdout, stderr = ssh.exec_command('groups')
                        groups = stdout.readlines()
#                       if len(groups) == 2:
                        for gr in groups:
                                print(gr)
#                       if 'administrators' in groups:
#                                       print("teacher:administrators TRUE")
#                       else:
#                               print("teacher:administrators FALSE")
#                        if 'teachers' in groups:
#                                       print("teacher:teachers TRUE")
#                       else:
#                               print("teacher:teachers FALSE")
#                       else:
#                               print("Teacher not included in the two groups. %i" % len(groups))
                except:
                        print('[!] EXCEPTION SSH')
        ssh.close()

# name_pod: [first_ip, second_ip, domain_h1, domain_h2, domain_www, domain_file, domain_mail, domain_monit, pod_id, slave_pod_id]
#		0	1		2	3	4		5		6	7		8	9
machines = {}
ip = int(111)
count_of_hosts = 19
for id_pod in range(1,count_of_hosts+1):
	name_pod = 'pod' + str(id_pod)
	first_ip = '192.168.88.' + str(ip)
	ip += 1
	second_ip = '192.168.88.' + str(ip)
	ip += 1
	domain_h1 = 'h1.' + name_pod + '.ru'
	domain_h2 = 'h2.' + name_pod + '.ru'
	domain_www = 'www.' + name_pod + '.ru'
	domain_file = 'file.' + name_pod + '.ru'
	domain_mail = 'mail.' + name_pod + '.ru'
	domain_monit = 'monit.' + name_pod + '.ru'
	pod_id = id_pod
	slave_pod_id = id_pod-1 if id_pod-1 else count_of_hosts
	machines.update( {name_pod: [first_ip, second_ip, domain_h1, domain_h2, domain_www, domain_file, domain_mail, domain_monit, pod_id, slave_pod_id]} )

teacher_ip = '192.168.88.100'

check_dns(machines, teacher_ip)
check_web(machines)
'''
if len(sys.argv) >= 2:
	keys = ['pod'+str(sys.argv[i]) for i in range(1,len(sys.argv))]
for key in keys:
	print('[.]')
	print('[.] CHECK %s' % key)
	print('[.] Check SSH Servers')
	print('[.] Checking SSH into server 1:')
	check_ssh(machines[key][0], True)
	check_ssh(machines[key][0], False)
	print('[.] Checking SSH into server 2:')
	check_ssh(machines[key][1], True)
	check_ssh(machines[key][1], False)
'''
#if __name__ == '__main__':
#	if len(sys.argv) > 1:
#		testOne(format(sys.argv[1]), machines[format(sys.argv[1])][0], machines[format(sys.argv[1])][1])
#	else:
#		main()
#	check_dns()
